package xyz.xmatic.auth.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AppUser {
    private Integer id;
    private String username, password;
    private String role;
}
